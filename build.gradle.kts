// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.jetbrainsKotlinAndroid) apply false
}

buildscript {
    repositories {
        google() // Añade el repositorio de Google para obtener la dependencia de Google Services
    }

    dependencies {
        classpath ("com.google.gms:google-services:4.4.1")
    }
}
